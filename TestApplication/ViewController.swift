//
//  ViewController.swift
//  TestApplication
//
//  Created by Tom King on 07/08/2019.
//  Copyright © 2019 Tom King. All rights reserved.
//

import UIKit
import TestFramework

class ViewController: UIViewController
{
    override func viewDidLoad()
    {
        let helper = OpenCVHelper()
        
        // I would exptect this line to break when compiled/running as this application contains no reference to openCV and yet it works fine and it prints the result from grabcub from TestFrameowrk, therefore OpenCV must be getting bundled into the framework
        helper.calGrabcut()
    }
}

